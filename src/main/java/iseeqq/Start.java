
package iseeqq;

import java.awt.AWTException;
import java.io.File;

import org.apache.log4j.Logger;

import iseeqq.context.ISeeQQContext;
import iseeqq.context.ISeeqqExecutor;
import iseeqq.model.QQMsg;

/**
 * @author 戴永杰
 *
 * @date 2017年11月7日 下午4:24:11
 * @version V1.0
 *
 */
public class Start {

	private static Logger logger = Logger.getLogger(Start.class);

	public static void main(String[] args) throws InterruptedException, AWTException {

		ISeeqqExecutor iseeqqExecutor = new ISeeqqExecutor() {

			@Override
			public void onMessage(String name, QQMsg context) {
				logger.info("  窗口:" + name + "\t昵称:" + context.getNickName() + "\t账号:" + context.getAccount() + "\t类型"
						+ context.getType() + "\t时间:" + context.getTime());
				for (String string : context.getContext()) {
					logger.info("\t" + string);
				}
				logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				QQMsg msg = new QQMsg();
				if ("流浪儿".equals(name)) {
					msg.append("笨笨的机器人，自动回复:");
					for (Object string : context.getContext()) {
						if (string.toString().contains("IMG")) {
							String pockIMGSrc = msg.pockIMGSrc(string.toString());
							File file = new File(pockIMGSrc);
							msg.append(file).append("ok");
						}
					}
					writeQQMsg(msg);
				}
			}
		};

	}
}
