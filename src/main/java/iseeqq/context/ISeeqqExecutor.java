package iseeqq.context;

import java.awt.Robot;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import iseeqq.context.Listener.KeyListener;
import iseeqq.context.Listener.QQListener;
import iseeqq.model.QQMsg;

/**
 * 
 * 
 * @author 戴永杰
 *
 * @date 2017年11月9日 下午4:41:42
 * @version V1.0
 *
 */
public abstract class ISeeqqExecutor implements QQListener {

	private QQContext qqContext;

	public ISeeqqExecutor() {
		try {
			qqContext = new QQContext(new Robot());
			ISeeQQContext wintask = new ISeeQQContext(qqContext);
			wintask.setQqListener(this);
			Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(wintask, 0, 100, TimeUnit.MILLISECONDS);
			Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(Level.OFF);
			logger.setUseParentHandlers(false);
			try {
				GlobalScreen.registerNativeHook();
			} catch (NativeHookException e) {
				e.printStackTrace();
			}
			GlobalScreen.addNativeKeyListener(new KeyListener());
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 当前窗口进行回复
	 * 
	 * @param msg
	 */
	public void writeQQMsg(QQMsg msg) {
		qqContext.writeQQMsg(msg);
	}

	/**
	 * 当前窗口进行回复
	 * 
	 * @param msg
	 */
	public void writeQQMsg(String msg) {
		qqContext.writeQQMsg(new QQMsg().append(msg));
	}

	@Override
	public void onRedEnvelope(String window, QQMsg context) {
		// TODO Auto-generated method stub

	}


}
