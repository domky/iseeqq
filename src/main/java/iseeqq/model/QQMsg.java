package iseeqq.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 目前支持 文字和图片
 * 
 * @author 戴永杰
 *
 * @date 2017年11月8日 下午4:16:51 
 * @version V1.0   
 *
 */
public class QQMsg {
	
	private List<Object> context = new ArrayList<>();

	/**
	 * 	0 未识别1 个人 2 群 3 讨论组
	 */
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	private String nickName;
	private String account;
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	private Date time;
	public QQMsg append(String str){
		context.add(str);
		return this;
	}
	
	public QQMsg append(File file){
		context.add(file);
		return this;
	}

	public List<Object> getALLContext() {
		return context;
	}

	/**
	 * 仅返回String形式是内容信息
	 * @return
	 */
	public List<String> getContext(){
		List<String> result = new ArrayList<>();
		context.forEach(x->{
			if(x instanceof String)
				result.add((String) x);
		});
		return result;
	}
	/**
	 * 这是一张图片 <IMG src="file:///C:\Users\ADMINI~1\AppData\Local\Temp\FZIETLV9DWB9R}D5S16G2VO.gif" sysface=212>
	 * @param str
	 * @return file:///C:\Users\ADMINI~1\AppData\Local\Temp\FZIETLV9DWB9R}D5S16G2VO.gif
	 */
	public String pockIMGSrc(String str){
		int indexOf = str.indexOf("<IMG");
		if(indexOf!=-1){
		  String imgStr = str.substring(indexOf,str.indexOf(">", indexOf)+1);
		  indexOf = imgStr.indexOf("src=")+5;
		  return imgStr.substring(indexOf,imgStr.indexOf("\"",indexOf)).replace("file:///", "");
		}
		return "";
		
	}
}
