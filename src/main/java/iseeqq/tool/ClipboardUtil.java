package iseeqq.tool;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.IOException;

import iseeqq.model.QQMsg;

/***
 * 
 * 
 * @author 戴永杰
 *
 * @date 2017年11月14日 下午5:37:17
 * @version V1.0
 *
 */
public class ClipboardUtil {

	public static void clear() {
		try {
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(null), null);
		} catch (Exception e) {
		}

	}

	public static void main(String[] args) {
		ClipboardUtil.clear();
	}

	public static void setClipboardImage(final Image image) {
		Transferable trans = new Transferable() {
			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				if (isDataFlavorSupported(flavor)) {
					return image;
				}
				throw new UnsupportedFlavorException(flavor);
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {
				return new DataFlavor[] { DataFlavor.imageFlavor };
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return DataFlavor.imageFlavor.equals(flavor);
			}
		};

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(trans, null);
	}

	public static QQMsg getSystemClipboardQQ() {

		try {
			Clipboard sysClb = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable t = sysClb.getContents(null);
			String subType = t.getTransferDataFlavors()[0].getSubType();

			BufferedReader br = null;
			StringBuilder sbStr = new StringBuilder();
			String line = null;
			switch (subType) {
			case "html":
				br = new BufferedReader(t.getTransferDataFlavors()[0].getReaderForText(t));
				while ((line = br.readLine()) != null) {
					sbStr.append(line);
				}
				return QQ.parseQQText(sbStr.toString());
			case "x-java-text-encoding":
				br = new BufferedReader(t.getTransferDataFlavors()[1].getReaderForText(t));
				while ((line = br.readLine()) != null) {
					sbStr.append(line);
				}
				return QQ.parseQQText(sbStr.toString());
			case "x-java-serialized-object":
				return new QQMsg();
			default:
				System.err.println("未识别的类型" + t.getTransferDataFlavors()[0].getSubType());
				br = new BufferedReader(t.getTransferDataFlavors()[0].getReaderForText(t));
				while ((line = br.readLine()) != null) {
					sbStr.append(line);
				}
				return QQ.parseQQText(sbStr.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	public static String getSystemClipboard() {
		Clipboard sysClb = null;
		sysClb = Toolkit.getDefaultToolkit().getSystemClipboard();
		try {

			Transferable t = sysClb.getContents(null);
			if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				String text = (String) t.getTransferData(DataFlavor.stringFlavor);
				return text;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void setSysClipboardText(String msg) {
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable tText = new StringSelection(msg);
		clip.setContents(tText, null);
	}
}
