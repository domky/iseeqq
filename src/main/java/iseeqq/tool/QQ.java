package iseeqq.tool;

import java.awt.image.BufferedImage;

import iseeqq.model.ImgCut;
import iseeqq.model.QQMsg;


/**
 * 
 * 
 * @author 戴永杰
 *
 * @date 2017年11月14日 下午5:37:32 
 * @version V1.0   
 *
 */
public class QQ {

	/**
	 * 
	 * @param img
	 * @return 0 未登录 1 在线
	 */
	public static int getQQStatus(BufferedImage img) {
		int online = 0; // 在线
		int offline = 0; // 离线

		int w = img.getWidth();
		int h = img.getHeight();
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int rgb = img.getRGB(i, j);
				// int[] rgb2 = Color.rgb(rgb);
				if (rgb == -1369049) { // 在线的RGB （235,28,39） 偏红
					online++;
				} else if (rgb == -9211021) { // 离线 （115,115,115） 偏灰
					offline++;
				}
			}
		}
		return online > offline ? 1 : 0;
	}
	
	/**
	 * 
	 * @param imgSour
	 * @return 0 未识别1 个人 2 群 3 讨论组
	 */
	public static int getQQType(BufferedImage imgSour){
		int type = 0;
//		BufferedImage qqImg = ImgUtil.touchRetouch(imgSour, new int[] { 222, 225, 230 }, 5);
		BufferedImage qqImg = ImgUtil.custBianyuan(imgSour); //增强版
		// 反色
		qqImg = ImgUtil.inverseImage(qqImg);

		ImgCut[] splitImgY = ImgUtil.shootShadowSimple(qqImg).splitImgY(100);
		if(splitImgY.length==2){
			if(splitImgY[0].getH()>splitImgY[1].getH()){ //上>下 个人
				type = 1;
			}else{ //群 或 讨论组
				ImgCut[] splitImgX = ImgUtil.shootShadowSimple(splitImgY[1].getImg()).splitImgX(40);
				if(splitImgX.length==2){
					
					ImgCut[] splitImgY1 = ImgUtil.shootShadowSimple(splitImgX[1].getImg()).splitImgY(20);
			
					if(splitImgY1.length ==0){ //左侧切割
						type = 3;
					}else{
						type = 2;
					}
				}else{
					type = 0;
				}
			}
		}else{
			type = 0;
		}
		return type;
	}

	public static QQMsg parseQQText(String qqmsg) {
		QQMsg qqmst = new QQMsg();
		qqmsg = qqmsg.replace("<!--StartFragment -->", "");
		qqmsg = qqmsg.replace("<DIV>", "");
		qqmsg = qqmsg.replace("</DIV>", "");
		qqmsg = qqmsg.replace("&nbsp;", " ");
		String[] split = qqmsg.split("<br>");
		for (String line : split) {
			if (line != null)
				qqmst.append(line);
		}
		return qqmst;

	}
	

}
