import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class FileGanzhi {

	
	public static void main(String[] args) throws Exception {
		
		
		
		File inputFile = new File("img/pthb.png");   
		BufferedImage sourceImage = ImageIO.read(inputFile);//读取图片文件  
		BufferedImage target = ImageIO.read(inputFile);//读取图片文件  
		
		int width= 8;  
		int height = 8;  
		// targetW，targetH分别表示目标长和宽  
		int type= sourceImage.getType();// 图片类型  
		BufferedImage thumbImage = null;  
		double sx= (double) width / sourceImage.getWidth();  
		double sy= (double) height / sourceImage.getHeight(); 

		// 将图片宽度和高度都设置成一样，以长度短的为准  
		if (true) {  
		      if(sx > sy) {  
		            sx= sy;  
		            width= (int) (sx * sourceImage.getWidth());  
		      }else {  
		            sy= sx;  
		            height= (int) (sy * sourceImage.getHeight());  
		      }  
		}  
		// 自定义图片  
		if (type== BufferedImage.TYPE_CUSTOM) { // handmade  
		     ColorModel cm = sourceImage.getColorModel();  
		     WritableRaster raster = cm.createCompatibleWritableRaster(width,height);  
		     boolean alphaPremultiplied = cm.isAlphaPremultiplied();  
		     thumbImage= new BufferedImage(cm, raster, alphaPremultiplied, null);  
		 } else {  
		     // 已知图片，如jpg，png，gif  
		     thumbImage= new BufferedImage(width, height, type);  
		}  
		// 调用画图类画缩小尺寸后的图  
		Graphics2D g = target.createGraphics();  
		//smoother than exlax:  
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);  
		g.drawRenderedImage(sourceImage,AffineTransform.getScaleInstance(sx, sy));  
		g.dispose();  
		
		int[]pixels = new int[width * height];  
		for (int i = 0; i < width; i++) {  
		      for(int j = 0; j < height; j++) {  
		            pixels[i* height + j] = rgbToGray(thumbImage.getRGB(i, j));  
		      }  
		}  
		
		int avgPixel= 0;  
		int m = 0;  
		for (int i =0; i < pixels.length; ++i) {  
		      m +=pixels[i];  
		}  
		m = m /pixels.length;  
		avgPixel = m;  
		
		int[] comps= new int[width * height];  
		for (int i = 0; i < comps.length; i++) {  
		    if(pixels[i] >= avgPixel) {  
		        comps[i]= 1;  
		    }else {  
		        comps[i]= 0;  
		    }  
		}  
		
		StringBuffer hashCode = new StringBuffer();  
		for (int i = 0; i < comps.length; i+= 4) {  
		      int result = comps[i] * (int) Math.pow(2, 3) + comps[i + 1] * (int) Math.pow(2, 2)+ comps[i + 2] * (int) Math.pow(2, 1) + comps[i + 2];  
		      hashCode.append(binaryToHex(result));//二进制转为16进制  
		}  
		String sourceHashCode = hashCode.toString();  
		System.out.println(sourceHashCode);
	}
	
	
	  /** 
     *  
     * @param bytes 
     * @return 将二进制数组转换为十六进制字符串  2-16
     */  
    public static String binaryToHex(int bytes){  

        String result = "";  
//        String hex = "";  
//        for(int i=0;i<bytes.length;i++){  
//            //字节高4位  
//            hex = String.valueOf(hexStr.charAt((bytes[i]&0xF0)>>4));  
//            //字节低4位  
//            hex += String.valueOf(hexStr.charAt(bytes[i]&0x0F));  
//            result +=hex;  //+" "
//        }  
        return result;  
    } 
	/**  
	 * 灰度值计算  
	 * @param pixels 彩色RGB值(Red-Green-Blue 红绿蓝)  
	 * @return int 灰度值  
	 */  
	public static int rgbToGray(int pixels) {  
	       // int _alpha =(pixels >> 24) & 0xFF;  
	       int _red = (pixels >> 16) & 0xFF;  
	       int _green = (pixels >> 8) & 0xFF;  
	       int _blue = (pixels) & 0xFF;  
	       return (int) (0.3 * _red + 0.59 * _green + 0.11 * _blue);  
	}  
}
